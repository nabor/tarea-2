package gimp.image.color;

public class GGrayQuad {
	private int x,y,w,h,gray;
	
	public GGrayQuad(int x, int y, int w, int h, int gray){
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.gray = gray;
	}
	
	public int getGray(){
		return this.gray;
	}
	public int getX(){
		return this.x;
	}
	public int getY(){
		return this.y;
	}
	public int getWidth(){
		return this.w;
	}
	public int getHeight(){
		return this.h;
	}
}
