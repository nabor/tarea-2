package gimp.image.color;

public class GColor {
	private int red;
	private int blue;
	private int green;
	
	public GColor(int red, int blue, int green) {
		this.red = red;
		this.blue = blue;
		this.green = green;
	}
	
	public int getRed() {
		return red;
	}
	public void setRed(int red) {
		this.red = red;
	}
	public int getBlue() {
		return blue;
	}
	public void setBlue(int blue) {
		this.blue = blue;
	}
	public int getGreen() {
		return green;
	}
	public void setGreen(int green) {
		this.green = green;
	}
	@Override
	public boolean equals(Object c){
		if(c instanceof GColor)
			return (this.red==((GColor)c).red)&&(this.green==((GColor)c).green)&&(this.blue==((GColor)c).blue);
		else
			return false;
	}
}
