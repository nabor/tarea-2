package gimp.image;

import java.io.IOException;
import java.io.OutputStream;

public class GImageWriter {
	
	public void write(GImage image, OutputStream stream) throws IOException{
		String firstLine= image.width()+","+image.height()+"\n";
		stream.write(firstLine.getBytes());
		for(int x=0;x<image.width();x++){
			for(int y=0;y<image.height();y++){
				String line=x+","+y+","+image.getPixel(x, y).getBlue()+"\n";
				stream.write(line.getBytes());
			}
		}
		
	}
}
