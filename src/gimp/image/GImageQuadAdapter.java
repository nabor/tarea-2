package gimp.image;

import gimp.image.color.*;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Iterator;

public class GImageQuadAdapter extends GImage {

	private BufferedImage image;
	private ArrayList<GGrayQuad> quads = new ArrayList<GGrayQuad>();
	
	public GImageQuadAdapter(Image img){
		image = new BufferedImage( img.getWidth(null), img.getHeight(null),  BufferedImage.TYPE_INT_ARGB);
		Graphics g = image.getGraphics();
	    g.drawImage(img, 0, 0, null);
	}
	
	public GImageQuadAdapter(int width,int height){
		image = new BufferedImage( width, height,  BufferedImage.TYPE_INT_ARGB);
	}
	
	public void QuadsToImage(){
		for (Iterator<GGrayQuad> quad = quads.iterator(); quad.hasNext();) {
			GGrayQuad grayQuad = (GGrayQuad) quad.next();
			int gray = grayQuad.getGray();
			int x = grayQuad.getX();
			int y = grayQuad.getY();
			for (int i = 0; i < grayQuad.getWidth(); i++) {
				for (int j = 0; j < grayQuad.getHeight(); j++) {
					this.setPixel(new GColor(gray,gray,gray), x, y);
				}
			}
		}
	}
	public void addQuad(int x, int y, int w, int h, int gray){
		this.quads.add(new GGrayQuad(x, y, w, h, gray));
	}

	@Override
	public int height() {
		return  image.getHeight(null);
	}

	@Override
	public int width() {
		return image.getWidth(null);
	}

	@Override
	public void setPixel(GColor c, int x, int y) {
		Color javaColor = new Color(c.getRed(),c.getGreen(),c.getBlue()); 
		image.setRGB(x, y, javaColor.getRGB());
	}

	@Override
	public GColor getPixel(int x, int y) {
		Color javaColor= new Color(image.getRGB(x, y));
		return new GColor(javaColor.getRed(),
				javaColor.getBlue(),
				javaColor.getGreen());
	}

	@Override
	public void draw(Graphics g) {
		g.drawImage(image,0,0,null);
	}
}
