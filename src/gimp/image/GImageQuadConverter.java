package gimp.image;

public class GImageQuadConverter extends GImageConverter {
	
	@Override
	public GImage toGrayScale(GImage source){
		GImage gray = super.toGrayScale(source);
		GImage quad = new GImageQuadAdapter(gray.width(), gray.height());
		
		Quadrify(gray, 0,0,quad.width(),quad.height(),((GImageQuadAdapter)quad));
		((GImageQuadAdapter)quad).QuadsToImage();
		return quad;
	}
	private void Quadrify(GImage gray, int x, int y, int w, int h, GImageQuadAdapter quad){
		if(x+w > gray.width() || y+h > gray.height())
			return;
		if(w == 0 || h == 0)
			return;
		if (w == 1 && h == 1) {
			quad.addQuad(x, y, w, h, (gray.getPixel(x, y)).getRed());
			return;
		}
		int median = Median(x, y, w, h, gray);
		if(Variance(x, y, w, h, gray, median)< 5){
			quad.addQuad(x, y, w, h, median);
			return;
		};
		int x2 = x+w/2;
		int y2 = y+h/2;
		int w1 = w/2;
		int w2;
		int h1 = h/2;
		int h2;
		if(w%2 == 0){
			w2 = w/2;
		}else{
			w2= 1+w/2;
		}
		if(h%2 == 0){
			h2 = h/2;
		}else{
			h2 = 1+h/2;
		}
		Quadrify(gray, x, y, w1, h1, quad);
		Quadrify(gray, x2, y, w2, h1, quad);
		Quadrify(gray, x, y2, w1, h2, quad);
		Quadrify(gray, x2, y2, w2, h2, quad);
	}
	private int Median(int x, int y, int w, int h, GImage gray) {
		int med = 0;
		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				med += (gray.getPixel(x+i, y+j)).getRed();
			}
		}
		return med/(w*h);
	}
	private int Variance(int x, int y, int w, int h, GImage gray, int median) {
		int sum = 0;
		for (int i = 0; i < w; i++) {
			for (int j = 0; j < h; j++) {
				sum += Math.pow((median-(gray.getPixel(x+i, y+j)).getRed()),2);
			}
		}
		return sum/(w*h);
	}
}
