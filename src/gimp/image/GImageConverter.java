package gimp.image;

import gimp.image.color.GColor;

public class GImageConverter {

	public GImage toGrayScale(GImage source){
		GImage gray=new GImageAdapter(source.width(),source.height());
		for(int x=0; x< source.width();x++){
			for(int y=0; y < source.height();y++){
				gray.setPixel(toGray(source.getPixel(x, y)), x, y);	
			}
		}
		return gray;
	}
	
	public GColor toGray(GColor color){
		//0.299R + 0.587G + 0.114B
		int gray=(int)Math.round( 0.299*color.getRed()+ 0.587* color.getGreen()+ 0.114* color.getBlue());
		return new GColor(gray,gray,gray);
	}
}
