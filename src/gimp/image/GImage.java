package gimp.image;

import java.awt.Graphics;
import gimp.image.color.GColor;

public abstract class GImage {
	
	@Override
	public boolean equals(Object o){
		if(o instanceof GImage){
			GImage c = (GImage)o;
			for (int i = 0; i < c.height(); i++) {
				for (int j = 0; j < c.width(); j++) {
					if(!this.getPixel(i, j).equals(c.getPixel(i, j)))
						return false;
				}
			}
			return true;
		}else{
			return false;
		}
	}
	public abstract GColor getPixel(int x,int y);
	public abstract void  setPixel(GColor c,int x,int y);
	public abstract int height();
	public abstract int width();
	public abstract void draw(Graphics g);
}
