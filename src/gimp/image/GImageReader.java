package gimp.image;

import gimp.image.color.GColor;

import java.io.InputStream;
import java.util.Scanner;

public class GImageReader {

	public GImage read(InputStream stream){
		Scanner in=new Scanner(stream);
		String[] firstLine=in.nextLine().split(",");
		int width= Integer.parseInt(firstLine[0]);
		int height= Integer.parseInt(firstLine[1]);
		GImage img=new GImageAdapter(width,height);
		while(in.hasNext()){
			String[] data=in.nextLine().split(",");
			int x=Integer.parseInt(data[0]);
			int y=Integer.parseInt(data[1]);
			int gray=Integer.parseInt(data[2]);
			img.setPixel(new GColor(gray,gray,gray), x, y);
		}
		return img;
	}
}
