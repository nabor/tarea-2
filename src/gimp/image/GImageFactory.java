package gimp.image;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javax.swing.ImageIcon;

public class GImageFactory {
	public static GImage create(String path) throws FileNotFoundException{
		if(path.endsWith("jpg")){
			return new GImageAdapter(new ImageIcon(path).getImage());
		}else{
			return (new GImageReader().read(new FileInputStream(path)));
		}
	}
}
