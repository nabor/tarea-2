package gimp.ui;

import gimp.image.*;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class GFrame extends JFrame{
	
	private GImage image;
	public GFrame(){
		super("DIMP");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(100,100);
		image= new GImageAdapter(100,100);
		getContentPane().setLayout(new BorderLayout());
		loadMenu();
	}
	private void loadMenu(){
		JMenuBar menuBar = new JMenuBar();
	    menuBar.add(buildFileMenu());
	    menuBar.add(buildToGray());
	    setJMenuBar(menuBar);    
	}
	private JMenu buildFileMenu(){
		JMenu fileMenu = new JMenu("File");
	    JMenuItem openMenuItem = new JMenuItem("Open");
	    openMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				open();
			}
		});
	    JMenuItem saveMenuItem = new JMenuItem("Save as JPS");
	    saveMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				save();
			}
		});
	    fileMenu.add(openMenuItem);
	    fileMenu.add(saveMenuItem);
	    return fileMenu;
	}
	private JMenu buildToGray(){
		JMenu grayMenu = new JMenu("ToGray");
	    JMenuItem basicMenuItem = new JMenuItem("Basic");
	    basicMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				basicToGray();
			}
		});
	    JMenuItem quadMenuItem = new JMenuItem("QuadTree");
	    quadMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				quadToGray();
			}
		});
	    grayMenu.add(basicMenuItem);
	    grayMenu.add(quadMenuItem);
	    return grayMenu;
	}
	
	private void basicToGray(){
		image=new GImageConverter().toGrayScale(image);
		update();
	}
	
	private void quadToGray(){
		image=new GImageQuadConverter().toGrayScale(image);
		update();
	}
	
	private void update(){
		getContentPane().removeAll();
	  	getContentPane().add(new GCanvas(image),BorderLayout.CENTER);
	      pack(); 
	}
	
	private void open(){
		JFileChooser fileopen = new JFileChooser();
		fileopen.addChoosableFileFilter(new FileNameExtensionFilter("jpg files", "jpg"));
		fileopen.addChoosableFileFilter(new FileNameExtensionFilter("jps files", "jps"));
	    int ret = fileopen.showOpenDialog(this);
	    
	    if (ret == JFileChooser.APPROVE_OPTION) {
	      File file = fileopen.getSelectedFile();
	      try {
	    	  	image=GImageFactory.create(file.getCanonicalPath());
	    	  	update();
	      } catch (IOException e) {}
	    }
	}
	
	private void save(){
		JFileChooser filesave = new JFileChooser();
	    int ret = filesave.showSaveDialog(this);
	    
	    if (ret == JFileChooser.APPROVE_OPTION) {
	      File file = filesave.getSelectedFile();
	      try {
			(new GImageWriter()).write(image,new FileOutputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    }
	}
	
}
