package gimp.ui;

import gimp.image.GImage;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JPanel;

public class GCanvas extends JPanel{

	private GImage image;
	
	public GCanvas(GImage image) {
		this.image = image;
		Dimension size=new Dimension(image.width(), image.height());
		setMinimumSize(size);
		setPreferredSize(size);
		setMaximumSize(size);
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g); 
		//Graphics2D d= (Graphics2D)g;
		//d.scale(100,100);
		image.draw(g);
	}
	
}
