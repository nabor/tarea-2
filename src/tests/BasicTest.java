package tests;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import gimp.image.*;
import gimp.image.color.GColor;

import org.junit.*;

public class BasicTest {

	private GImage source;
	private GImage target;
	
	@Before
	public void setUp() throws Exception {
		source=buildSource();
		target=buildTarget();
	}
	
	@Test
	public void testEquals() {
		assertEquals(buildSource(),buildSource());
		assertFalse(buildTarget().equals(buildSource()));
	}
	
	@Test
	public void testConverter() {
		GImageConverter ic=new GImageConverter();
		assertEquals(ic.toGrayScale(source),target);
	}
	
	@Test
	public void testColorConverter() {
		GColor color=new GColor(80,20,70);
		GColor gray =new GColor(67,67,67);
		GImageConverter ic=new GImageConverter();
		assertEquals(gray,ic.toGray(color));
	}
	
	@Test
	public void testReadWrite() throws IOException{
		GImageWriter writer=new GImageWriter();
		GImageReader reader=new GImageReader();
		String source= "2,2\n0,0,44\n0,1,35\n1,0,89\n1,1,255\n";
		GImage image=reader.read(new ByteArrayInputStream(source.getBytes()));
		OutputStream out=new ByteArrayOutputStream();
		writer.write(image,out);
		assertEquals(source,out.toString());
	}
	
	
	
	@Test(expected = Exception.class)
	public void testMalformedGImage() {  
		GImageReader reader=new GImageReader();
		String source= "2,2\n0,3,44\n0,1,35\n1,0,89\n1,1,255\n";
		reader.read(new ByteArrayInputStream(source.getBytes()));
	}
	
	@Test(expected = Exception.class)
	public void testFormatError() {  
		GImageReader reader=new GImageReader();
		String source= "2,2   0,3,44   0,1,35\n1,0,89\n1,1,255\n";
		reader.read(new ByteArrayInputStream(source.getBytes()));
	} 
	
	
	public GImage buildSource(){
		GImage img=new GImageAdapter(4,4);
		img.setPixel(new GColor(0,0,255),0,0);
		img.setPixel(new GColor(0,0,255),1,0);
		img.setPixel(new GColor(0,0,255),0,1);
		img.setPixel(new GColor(0,0,255),1,1);
		
		img.setPixel(new GColor(0,255,0),2,0);
		img.setPixel(new GColor(0,255,0),3,0);
		img.setPixel(new GColor(0,255,0),2,1);
		img.setPixel(new GColor(0,255,0),3,1);
		
		img.setPixel(new GColor(255,0,0),2,2);
		img.setPixel(new GColor(255,0,0),2,3);
		img.setPixel(new GColor(255,0,0),3,2);
		img.setPixel(new GColor(255,0,0),3,3);
		
		img.setPixel(new GColor(255,255,255),0,2);
		img.setPixel(new GColor(255,255,255),1,3);
		img.setPixel(new GColor(255,255,255),0,3);
		img.setPixel(new GColor(255,255,255),1,2);
		return img;
	}
	public GImage buildTarget(){
		// 0.299R + 0.587G + 0.114B
		GImage img=new GImageAdapter(4,4);
		img.setPixel(new GColor(150,150,150),0,0);
		img.setPixel(new GColor(150,150,150),1,0);
		img.setPixel(new GColor(150,150,150),0,1);
		img.setPixel(new GColor(150,150,150),1,1);
		
		img.setPixel(new GColor(29,29,29),2,0);
		img.setPixel(new GColor(29,29,29),3,0);
		img.setPixel(new GColor(29,29,29),2,1);
		img.setPixel(new GColor(29,29,29),3,1);
		
		img.setPixel(new GColor(76,76,76),2,2);
		img.setPixel(new GColor(76,76,76),2,3);
		img.setPixel(new GColor(76,76,76),3,2);
		img.setPixel(new GColor(76,76,76),3,3);
		
		img.setPixel(new GColor(255,255,255),0,2);
		img.setPixel(new GColor(255,255,255),1,3);
		img.setPixel(new GColor(255,255,255),0,3);
		img.setPixel(new GColor(255,255,255),1,2);
		
		return img;
	}
}
